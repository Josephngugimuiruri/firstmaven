package org.mik.first.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Person;

public class PersonService implements Service<Person> {

    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    @Override
    public void pay(Person client) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter PersonService. client:"+client);
        System.out.println("Enter PersonService. client:"+client);
    }

    @Override
    public void receiveService(Person client) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter PersonService.receiveService client:" + client);
        System.out.println("Enter PersonService.receiveService client:" + client);
    }
}
