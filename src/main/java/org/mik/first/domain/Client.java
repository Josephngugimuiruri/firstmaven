package org.mik.first.domain;

import org.apache.commons.lang3.StringUtils;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerializable;

import java.util.Objects;

@XMLSerializable(key="MyClient")
public abstract class Client {

    @XMLElement(key="Identifier")
    private Long id;
    @XMLElement
    private String name;
    @XMLElement
    private String address;

    public Client() {

    }

    public Client(Long id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name==null || name.isEmpty() || StringUtils.isBlank(name))
            return;

        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;

        Client client = (Client) o;

        if (id != null ? !id.equals(client.id) : client.id != null) return false;
        if (!name.equals(client.name)) return false;
        return address.equals(client.address);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + address.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
