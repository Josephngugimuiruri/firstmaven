package org.mik.first.domain;

import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerializable;

@XMLSerializable
public class Person extends Client {

    @XMLElement(key = "PersonalId")
    private String idNumber;



    public Person() {
        super();
    }

    public Person(String idNumber) {
        this.idNumber = idNumber;
    }

    public Person(Long id, String name, String address, String idNumber) {
        super(id, name, address);
        this.idNumber = idNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        if (!super.equals(o)) return false;

        Person person = (Person) o;

        return idNumber.equals(person.idNumber);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + idNumber.hashCode();
        return result;
    }

    public String toString() {
        return "Person{" +
                "idNumber='" + idNumber + '\'' +
                super.toString() +
                '}' ;
    }
}
