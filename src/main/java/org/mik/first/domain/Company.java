package org.mik.first.domain;

import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerializable;

@XMLSerializable
public class Company extends Client {

    @XMLElement
    private String taxNumber;

    public Company() {
    }

    public Company(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public Company(Long id, String name, String address, String taxNumber) {
        super(id, name, address);
        this.taxNumber = taxNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        if (!super.equals(o)) return false;

        Company company = (Company) o;

        return taxNumber.equals(company.taxNumber);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + taxNumber.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Company{" +
                "taxNumber='" + taxNumber + '\'' +
                super.toString() +
                '}';
    }
}
